<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string  title
 * @property string  price
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'price',
    ];

    public function offers()
    {
        return $this->belongsToMany(Offer::class);
    }
}
