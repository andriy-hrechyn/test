<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string  email
 */
class OfferUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'email'
    ];

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}
