<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateOfferRequest;
use App\Http\Resources\OfferResource;
use App\Models\Offer;
use App\Models\OfferUser;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

/**
 * Class OfferController
 * @package App\Http\Controllers
 */
class OfferController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return OfferResource::collection(Offer::all());
    }

    /**
     * @param int $offerId
     *
     * @return OfferResource
     */
    public function show(int $offerId)
    {
        return OfferResource::make(Offer::query()->find($offerId));
    }

    /**
     * @param UpdateOfferRequest $request
     *
     * @return OfferResource
     */
    public function update(UpdateOfferRequest $request)
    {
        /** @var OfferUser $user */
        $user = OfferUser::query()->firstOrCreate(['email' => $request->input('user.email')]);

        /** @var Offer $offer */
        $offer = Offer::query()->updateOrCreate(['id' => $request->route('id')],[
            'title' => $request->input('title'),
            'user_id' => $user->id
        ]);

        $offer->products()->sync($request->input('productsIds'));

        return OfferResource::make($offer);
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     */
    public function delete(Request $request)
    {
        $offer = Offer::query()->find($request->route('id'));

        $offer->delete();

        return response()->noContent();
    }
}
