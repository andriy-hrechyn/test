<?php

namespace App\Http\Resources;

use App\Models\OfferUser;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferUserResource extends JsonResource
{
    /**
     * @var OfferUser
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->resource->id,
            'email' => $this->resource->email,
        ];
    }
}
