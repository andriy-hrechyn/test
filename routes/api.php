<?php

use App\Http\Controllers\OfferController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/offers', [OfferController::class, 'index']);

Route::get('/offers/{id}', [OfferController::class, 'show']);

Route::match(['post', 'put'], '/offers/{id?}', [OfferController::class, 'update']);

Route::delete('/offers/{id}', [OfferController::class, 'delete']);

